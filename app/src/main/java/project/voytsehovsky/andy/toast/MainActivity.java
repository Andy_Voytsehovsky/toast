package project.voytsehovsky.andy.toast;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText password;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListengerOnButton();
    }

    public void addListengerOnButton(){
        password = (EditText)findViewById(R.id.editText);
        btn = (Button)findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn.setText("Up");
                btn.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                Toast.makeText(MainActivity.this, password.getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
